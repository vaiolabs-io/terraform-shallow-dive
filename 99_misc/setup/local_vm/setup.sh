#!/usr/bin/env bash 
###########################################
# Created by: Alex M. Schapelle AKA Silent-Mobius
# Purpose: Setup VM/Workstation with  terraform on it
# Version: 0.0.1
# Date: 24/07/2024
set -x 
set -o errexit
set -o pipefail
. /etc/os-release
###########################################
NULL=/dev/null
RED="\033[01;31m"
GREEN="\033[01;32m"
YELLOW="\033[01;34m"
NC="[\033[0m"

function main(){
    if [[ $EUID != 0 ]];then
        warn "Please Escalate Privileges"
        exit 1
    else
        if [[ $ID == 'debian' ]] || [[ $ID == 'ubuntu' ]] || [[ $ID == 'linuxmint' ]];then
            if which wget > $NULL 2>&1;then
                info "Getting Terrform Repository"
                    wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
                    echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
                        tee /etc/apt/sources.list.d/hashicorp.list
                info "Installing Terrform"
                    apt-get update &&  apt-get install -y terraform
            else
                warn "Missing wget utility, Please install to continue"
                exit 1
            fi
                
                setup_work_folder
        fi
        if [[ $ID == 'fedora' ]] || [[ $ID == 'rockylinux' ]];then
            yum install -y yum-utils
            yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
            yum -y install terraform

            setup_work_folder
        fi

        unsupported

    fi

}


function note(){
    local IN=$1
    printf "$YELLOW[NOTE] %s \n$NS" $IN
}

function info(){
    local IN=$1
    printf "$GREEN[INFO] %s \n$NC" $IN
}

function warn(){
    local IN=$1
    printf "$RED[WARN] %s \n$NC" $IN
}

function setup_work_folder(){
    mkdir -p /home/$USER/Projects/terraform/docker
    cd /home/$USER/Projects/terraform/docker
        echo '
        terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
      version = "~> 3.0.1"
    }
  }
}

provider "docker" {}

resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = false
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.image_id
  name  = "tutorial"
  ports {
    internal = 80
    external = 8000
  }
}
        
        ' > main.tf
    terraform init
}


function unsupported(){
    warn "Unfortunatelly this system is not supported"
    warn "Contact developer or your instructor for help/support"
    exit 1
}
######
# main - _- _- _- _- _- _- _- _- _- _ DO NOT REMOVE _- _- _- _- _- _- _- _- _- _- _- _- _
######
main "$@"

