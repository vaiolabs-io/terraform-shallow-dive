# Terraform Shallow Dive

<img src="../99_misc/.img/terraform.png" alt="terraform" style="float:right;width:300px;">


.footer: Created By Alex M. Schapelle, VAioLabs.io

<!--
 # Created By: Silent-Mobius Aka Alex M. Schapelle
# Purpose: being lazy on new course setup
# Copyright (C) 2023  Alex M. Schapelle

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 -->
---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is Terraform ?
- Who needs TerraformS ?
- How Terraform works ?
- How to manage Terraform in various scenarios ?


---

# Who Is This Course For ?

- The name kind of mentions it:
    - System administrators who wish to learn basic Terraform usage.
    - SysOps who are moving to DevOps jobs.
    - DevOps who wish to implement infrastructure as a code (IaC) with declarative language.
- But it also can be useful for:
    - Junior DevOps who wish to gain minimal knowledge of IaC.
    - Junior software developers who have no knowledge of IaC.

---

# Course Prerequisite

- TCP/IP knowledge - MUST
- UNIX/Linux shell usage - MUST
- Mild knowledge of shell scripting
- Moderate understanding of version control (git/github/gitlab) - Recommended
- Low-headed familiarity with containers (docker)

---

# Course Topics

- [Setup](../01_setup/README.md)
- [Fundamentals](../02_fundamentals/README.md)
- [Yaml Basics](../03_yaml_basics/README.md)
- [Playbooks](../04_playbooks/README.md)
- [Advance Execution](../05_advance_execution/README.md)
- [Roles](../06_roles/README.md)
- [Troubleshooting](../07_troubleshooting/README.md)

---

# About Me

<img src="../99_misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;">

- Over 15 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        -  A+.
        -  Cisco CCNA.
        -  RedHat RHCSA.
        -  LPIC1 and Shell scripting.
        -  Other stuff I've learned by myself.

---

# About Me (cont.)

- Over 7 years of sysadmin and automation:
    - Shell scripting fanatic
    - Python developer
    - JS admirer
    - Golang fallen
    - Rust fan
- 8 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user
    - Terraform informant

---

# About Me (cont.)

You can find me on the internet in bunch of places:

- Linkedin: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)
- Gitlab: [Silent-Mobius](https://gitlab.com/silent-mobius)
- Github: [Zero-Pytagoras](https://github.com/zero-pytagoras)
- ASchapelle: [My Site](https://aschapelle.com)
- VaioLabs-IO: [My company site](https://vaiolabs.io)


---

# About You

Share some things about yourself:

- Name and surname
- Job description
- What type of education do you poses ? formal/informal/self-taught/university/cert-course
- Do you know any of those technologies below ? What level ?
    - Docker / Docker-Compose / K8s
    - Jenkins
    - Git / GitLab / Github / Gitea / Bitbucket
    - Bash/PowerShell Script
    - Python3 / Pytest / Pylint / Flask
    - Go / Gin / Echo
    - Ansible / Terraform
- Do you have any hobbies ?
- Do you pledge your alliance to [Emperor of Man kind](https://warhammer40k.fandom.com/wiki/Emperor_of_Mankind) ?

---

# History


<img src="../99_misc/.img/pc-history.png" alt="pc history" style="float:right;width:300px;">

Many developers and system administrators manage servers by logging into them via SSH, making changes, and logging off. Some of these changes would be documented, some would not. If an admin needed to make the same change to many servers, the admin would manually log into each server and repeatedly make this change.

Some admins may use shell scripts to try to reach some level of sanity, but I’ve yet to see a complex shell script that handles all edge cases correctly while synchronizing multiple servers’ configuration and deploying new code

But there’s a reason why many developers and sysadmins stick to shell scripting and command-line configuration: it’s simple and easy-to-use, and they’ve had years of experience using bash and command-line tools. Why throw all that out the window and learn a new configuration language and methodology?

---

# History (cont.)

#### Infrastructure As Code (IaC)

Infrastructure as code (IaC) is the process of managing and provisioning computer data center resources through machine-readable definition files, rather than physical hardware configuration or interactive configuration tools. 
IaC grew as a response to the difficulty posed by utility computing and second-generation web frameworks. In 2006, the launch of Amazon Web Services’ Elastic Compute Cloud and the 1.0 version of Ruby on Rails just months before created widespread scaling difficulties in the enterprise that were previously experienced only at large, multi-national companies. With new tools emerging to handle this ever-growing field, the idea of IaC was born

---

# History (cont.)

#### Enter Terraform

<img src="../99_misc/.img/terraform.png" alt="Terraform_logo" style="float:right;width:180px;">

Unfortunately there is no saucy history behind it, besides the following bits provided by wikipedia, IBM forums (of all places), terraform site itself and chatgpt:

#### Origins

Terraform was created by Mitchell Hashimoto and eventually by Hashicorp, a company known for developing infrastructure automation tools. It was first released by HashiCorp in July 2014, initially as an open-source project. 

The Mitchell identified a growing need for a tool that could automate the provisioning and management of infrastructure across various cloud providers and other services. Before Terraform, managing infrastructure typically involved manual processes or scripting specific to each provider, which made scalability and consistency challenging.


---
<!-- # https://www.ibm.com/topics/terraform -->
# What Is Terrform?

<img src="../99_misc/.img/terraform_gears.png" alt="Terraform_Gears_logo" style="float:right;width:90px;">

Terraform is a declarative coding tool that enables developers to use a high-level configuration language called HashiCorp Configuration Language (HCL) to describe the desired “end-state” cloud or on-premises infrastructure for running an application. It then generates a plan for reaching that end-state and runs the plan to provision the infrastructure. 
Terraform uses a simple syntax, provisions infrastructure across multiple cloud and on-premises data centers and safely and efficiently re-provision infrastructure in response to configuration changes. This is why it is currently one of the most popular infrastructure automation tools available. If your organization plans to deploy a hybrid cloud or multi-cloud environment, you’ll likely want or need to get to know Terraform.

---

# Why  Terraform ?

There are a few key reasons developers choose to use Terraform over other Infrastructure as Code tools:

- __Open source__: Terraform is backed by large communities of contributors who build plug-ins to the platform. Regardless of which cloud provider you use, it’s easy to find plug-ins, extensions and professional support. This also means Terraform evolves quickly, with new benefits and improvements added consistently.

- __Platform agnostic__: This means you can use it with any cloud services provider. Most other IaC tools are designed to work with single cloud provider.

- __Immutable infrastructure__: Most Infrastructure as Code tools create mutable infrastructure, meaning the infrastructure that can change to accommodate changes such as a middleware upgrade or a new storage server. The danger with mutable infrastructure is configuration drift. This means that as the changes pile up, the actual provisioning of different servers or other infrastructure elements ‘drifts’ further from the original configuration, making bugs or performance issues difficult to spot and fix. 

---

# Terraform modules

Terraform modules are small, reusable Terraform configurations for multiple infrastructure resources that are used together. Terraform modules are useful because they allow complex resources to be automated with reusable, configurable constructs. Writing even a very simple Terraform file results in a module. A module can call other modules—called child modules—which can make assembling configuration faster and more concise. Modules can also be called multiple times, either within the same configuration or in separate configurations.

---

# Terraform Providers

Terraform providers are plugins that implement resource types. Providers contain all the code needed to authenticate and connect to a service—typically from a public cloud provider—on behalf of the user. You can find providers for the cloud platforms and services you use, add them to your configuration and then use their resources to provision infrastructure. Providers are available for nearly every major cloud provider, SaaS offering and more, developed or supported by the Terraform community or individual organizations. Refer to the Terraform documentation (link resides outside ibm.com) for a detailed list.


---

# Terraform Vs. Ansible

Terraform and Ansible are both Infrastructure as Code tools, but there are a couple significant differences between the two:

While Terraform is purely a declarative tool (see above), Ansible® combines both declarative and procedural configuration. In procedural configuration, you specify the steps, or the precise manner, in which you want to provision infrastructure to the desired state. Procedural configuration is more work but it provides more control.

---

# License change

Terraform was previously free software available under version 2.0 of the Mozilla Public License (MPL). On August 10, 2023, HashiCorp announced that all products produced by the company would be relicensed under the Business Source License (BSL), with HashiCorp prohibiting commercial use of the community edition by those who offer "competitive services". 
The last MPL-licensed version of Terraform was forked as "OpenTofu", which is backed by the Linux Foundation. In April 2024


---

# Conclusion

The history of Terraform reflects the evolution of IaC practices towards automation, scalability, and consistency. Its development by HashiCorp and subsequent adoption by a wide range of organizations underscore its importance in modern DevOps and cloud-native environments.

