
---

# Terraform Basics

---

# Terraform Basics

Infrastructure as Code (IaC) tools allow you to manage infrastructure with configuration files rather than through a graphical user interface. IaC allows you to build, change, and manage your infrastructure in a safe, consistent, and repeatable way by defining resource configurations that you can version, reuse, and share.

Terraform is HashiCorp's infrastructure as code tool. It lets you define resources and infrastructure in human-readable, declarative configuration files, and manages your infrastructure's lifecycle. Using Terraform has several advantages over manually managing your infrastructure:
- Terraform can manage infrastructure on multiple cloud platforms.
- The (somewhat) human-readable configuration language helps you write infrastructure code quickly and reuse it.
- Terraform's `state` allows you to track resource changes throughout your deployments.
- You can commit your configurations to version control to safely collaborate on infrastructure.

--- 

# Terraform Basics

### API to the use case

Terraform plugins called providers let Terraform interact with cloud platforms and other services via their application programming interfaces (APIs). HashiCorp and the Terraform community have written over 1,000 providers to manage resources on Amazon Web Services (AWS), Azure, Google Cloud Platform (GCP), Kubernetes, Helm, GitHub, Splunk, and DataDog, just to name a few. Find providers for many of the platforms and services you already use in the Terraform Registry. If you don't find the provider you're looking for, you can write your own in according to HashiCorp community guidelines.

---

# Terraform Basics

### Deployment workflow

Providers define individual units of infrastructure, for example compute instances or private networks, as resources. You can compose resources from different providers into reusable Terraform configurations called modules, and manage them with a consistent language and workflow.

Terraform's configuration language is declarative, meaning that it describes the desired end-state for your infrastructure, in contrast to procedural programming languages that require step-by-step instructions to perform tasks. Terraform providers automatically calculate dependencies between resources to create or destroy them in the correct order

---

# Terraform Basics

### Deployment workflow

<img src="../99_misc/.img/terraform-flow.png" alt="pc history" style="float:right;width:300px;">

To deploy infrastructure with Terraform:

- __Scope__ : Identify the infrastructure for your project.
- __Author__ : Write the configuration for your infrastructure.
- __Initialize__ : Install the plugins Terraform needs to manage the infrastructure.
- __Plan__ : Preview the changes Terraform will make to match your configuration.
- __Apply__ : Make the planned changes.


---

# Terraform Basics

### Track your infrastructure

Terraform keeps track of your real infrastructure in a state file, which acts as a source of truth for your environment. Terraform uses the state file to determine the changes to make to your infrastructure so that it will match your configuration.

---

# Terraform Basics

### Providers

Terraform is a high-level orchestration tool, yet it does not mean it can do all the possible tasks, such as configuration management (CM). In such cases it is suggested to use terraform `providers`
to trigger the CM tasks with external tools, for example ansible.

> `[!]` Note: There are many CM tools, yet only ones that are multi-vendor are ansible and puppet. There are other such as CloudFormation, yet those are usually vendor locked and can not be used in all infrastructures.


---

# Terraform Basics

### Lab Setup

<img src="../99_misc/.img/setup.png" alt="lab setup" style="float:right;width:300px;">

---

# Lab Setup

### Easy way

if you are using Debian or RedHat based Linux distribution on your laptop/desktop/vm, then you may use [setup.sh](../99_misc/setup/local_vm/setup.sh) script, which will setup the project directory and initial lab files
In case you prefer container based environment, [install docker on your system](https://get.docker.com) and then under the '99_misc' folder of our course material we have [docker compose](../99_misc/setup/docker/) setup.
In case you are familiar with [Vagrant](https://vagrantup.com) then you may use our [Vagrant setup](../99_misc/setup/vagrant/)

---

# Lab Setup

### Not So Easy Way

##### Debian Based Setup

```sh
$ sudo apt-get update && sudo apt-get install -y gnupg software-properties-common
$ wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | \
    sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg > /dev/null
$ echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
        sudo tee /etc/apt/sources.list.d/hashicorp.list

$ terraform -help # verify that the installation worked
$ terraform -install-autocomplete
```
> `[!]` Note: Once the autocomplete support is installed, you will need to restart your shell.

---

# Lab Setup

### Not So Easy Way

##### Fedora/Rocky Based Setup

```sh
$ sudo dnf install -y dnf-plugins-core
$ sudo dnf config-manager --add-repo https://rpm.releases.hashicorp.com/fedora/hashicorp.repo
$ sudo dnf -y install terraform
$ terraform -help # verify that the installation worked
$ terraform -install-autocomplete
```

> `[!]` Note: Once the autocomplete support is installed, you will need to restart your shell.

---

# Lab Setup

### Not So Easy Way

##### Manual Binary Setup

Retrieve the terraform binary by [downloading a pre-compiled binary](https://releases.hashicorp.com/terraform/1.9.3/terraform_1.9.3_linux_amd64.zip)

> `[!]` Note: the link takes you to x86_64 binary. In case you are using ARM cpu, please check the [Official documentation link](https://developer.hashicorp.com/terraform/install)

```sh
$ cd ~/Downloads
$ wget https://releases.hashicorp.com/terraform/1.9.3/terraform_1.9.3_linux_amd64.zip
    # Version may vary, thus in case download fails, verify latest version on hashicorp website 
$ unzip  terraform_1.9.3_linux_amd64.zip
$ mv terraform /usr/local/bin/
$ echo $PATH  # to verify that we have /usr/local/bin/ in full PATH
$ terraform -help # verify that the installation worked
$ terraform -install-autocomplete
```

> `[!]` Note: Once the autocomplete support is installed, you will need to restart your shell.

---
# Terraform Workflow

<img src="../99_misc/.img/flow.jpg" alt="workflow" style="float:right;width:180px;">

---

# Initial Lab

```sh
mkdir -p ~/Projects/terraform-basics
cd ~/Projects/terraform-basics
touch main.tf
```

> `[!]` Note you need to have docker installed on you system

Open `main.tf` file write done the following code:

---

# Initial Lab (cont.)

```go
resource "docker_image" "nginx" {
  name = "nginx"
}

```

---

# Initial Lab (cont.)

Initialize the terraform files by running

```sh
terraform init
```
While running the command, terraform will read the `main.tf` file and will decide which api to download from the terraform registry. Once found, it will download the plugin and store it under the project folder within __hidden__ folder named `.terraform`. In this example above, we are using `virtualbox` provider which enables us use API to manage virtual machines on our localhost.

> `[!]` Note: in case you are running on-prem environment, please download the api's manually and save them on local registry provider such as sonatype nexus, jfrog artifactory or goHarbor

---

# Initial Lab (cont.)

To format the syntax of code written in the `main.tf` in a correct manner

```sh
terraform fmt
```

---

# Initial Lab (cont.)

To validate that the syntax of code written in the `main.tf` is correct, we use `validate` command

```sh
terraform validate
```

---

# Initial Lab (cont.)

To list the provider used in a project we will use `providers` command which will show the list of required providers

```sh
terraform providers
```
> `[!]` Note: in some cases, the listed providers will not be actually available, this happens due to various reasons, such as community provider not being maintained or provider developer changing the name without updating the terraform registry.(yes - it is wild in terraform community)

---

# Initial Lab (cont.)

To see what we have actually concatenated by the code above, we can use `plan` command that will describe what is about to happen with code we have used.

```sh
terraform plan
```

This step is crucial for you to verify what is about to happen on your IaC and in case something is not right, you may correct it.

> `[!]` Note: some of the parts of the output will be shown as `known after apply`, meaning to be determined after the terraform finishes the run

> `[!]` Note: in some cases output may be too long, thus use can use `-out` to drop the output to a separate file where you can review it manually

---

# Initial Lab (cont.)

To execute the plan provided before hand, we'll use `apply` that will execute the plan we have seen.

```sh
terraform apply
```

By the end of the run of terraform. several files will be generated, a `terraform.tfstate` file that keeps the track of what was done with help of terraform and what succeeded and a `vagrant.box` file that is equivalent of virtualbox OVA format.

> `[!]` Note: the `*tfstate` file is a critical file that needs to be protected at any cause. With out it terraform will try to overwrite the changes, which will damage the existing infrastructure.

---

# Initial Lab (cont.)

Once terraform finishes the run it will print out the summary of the task that were provided. if you wish to review them again, use `show` command

```sh
terraform show
```

---

# Initial Lab (cont.)

To get rid of all the existing infrastructure we'll be using `destroy` command, that will destroy everything that was saved under `*.tfstate` file

```sh
terraform destroy
```

> `[!]` Note: In cloud there might be some anomalies, thus it is suggested to check resources at the end of the run of `terraform destroy`

---

# Practice

Use the example above and setup project where we run terraform file which create vm on virtualbox with the code provided below

> Note: in order for this lab to work, you need to have physical machine with terraform and virtualbox installed. It might also work in virtual machine 

```go
terraform {
  required_providers {
    virtualbox = {
      source = "terra-farm/virtualbox"
      version = "0.2.2-alpha.1"
    }
  }
}
resource "virtualbox_vm" "node" {
  count     = 1
  name      = format("node-%02d", count.index + 1)
  image     = "https://app.vagrantup.com/generic/boxes/debian12/versions/4.3.12/providers/virtualbox/amd64/vagrant.box"
  cpus      = 2
  memory    = "512 mib"

  network_adapter {
    type           = "bridged"
    host_interface = "wlp1s0" # Notifce that this needs to be your physical or virtual network interface
    }
}

output "IPAddr" {
  value = element(virtualbox_vm.node.*.network_adapter.0.ipv4_address, 1)
}

output "IPAddr_2" {
  value = element(virtualbox_vm.node.*.network_adapter.0.ipv4_address, 2)
}

```

---

# Terraform Commands List Summary

Here is a list of terraform commands, that we have used. Some of them were not utilized by us yet, we'll do use them in the future.

- `apply`     : Builds or changes infrastructure
- `console`   : Interactive console
- `destroy`   : Destroys infrastructure
- `fmt`       : Rewrites config files in canonical format
- `get`       : Downloads and installs modules
- `graph`     : Creates a visual graph of resources
- `import`    : Imports existing infrastructure
- `init`      : Initializes a terraform configuration
- `output`    : Reads an output from state file
- `plan`      : Generates and shows the plan
- `providers` : prints the providers used
- `push`      : Uploads this terraform module
- `refresh`   : Updates local state file
- `show`      : Inspects terraform state file
- `taint`     : Manually marks a resource for recreation

---

# Terraform Configuration Language

Terraform's language is it's primary user interface. Configuration files you write in Terraform language tell Terraform what plugins to install, what infrastructure to create, and what data to fetch. 
The main purpose of the Terraform language is declaring resources, which represent infrastructure objects. All other language features exist only to make the definition of resources more flexible and convenient.
The explanation regarding the language can be found on [terraform documentation thus please RTFM](https://developer.hashicorp.com/terraform/language)

---

# Terraform Configuration Language

### Providers

Terraform relies on plugins called providers to interact with cloud providers, SaaS providers, and other APIs. Each provider adds a set of resource types and/or data sources that Terraform can manage. Every resource type is implemented by a provider; without providers, Terraform can't manage any kind of infrastructure. Providers are distributed separately from Terraform itself, and each provider has its own release cadence and version numbers. The Terraform [Registry](https://registry.terraform.io/browse/providers) is the main directory of publicly available Terraform providers, and hosts providers for most major infrastructure platforms.

---

# Terraform Configuration Language

### Provider Installation

- Terraform CLI finds and installs providers when initializing a working directory with `terraform init` command. It can automatically download providers from a Terraform registry, or load them from a local mirror or cache. If you are using a persistent working directory, you must reinitialize whenever you change a configuration's providers
- Terraform Enterprise install providers as part of every run
- To find providers for the infrastructure platforms you use, browse the providers section of the Terraform [Registry](https://registry.terraform.io/browse/providers)

- Example for the provider would be the `docker` provider we used during the initial explanation:

```go
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}
```
In the code above we are requiring from terraform to use the docker provider, with specific version. All those details, can be found on terraform [Registry](https://registry.terraform.io/browse/providers)

---

# Terraform Configuration Language

### Resource use

In the resource section, we are using `docker_image`  resource __type__ with name that we provide : `nginx`. In the block of code itself we create `reference` of the image with variable __name__ and write it down with required image name. According to documentation, terraform docker provider, will try to ask from docker daemon to use `nginx` image, and as a consequence, if the image is not located locally, it will pull the image from docker registry. 
Resource __type__ _is provided by the provider_ and you should always check with documentation (RTFM) in order to see the capabilities of the provider.

```go
# Pulls the image
resource "docker_image" "nginx" {
  name = "nginx"
}

```

---

# Terraform Configuration Language

### Meta-Argument Resources

The Terraform language defines the following meta-arguments, which can be used with any resource type to change the behavior of resources:

- `depends_on`, for specifying hidden dependencies
- `count`, for creating multiple resource instances according to a count
- `for_each`, to create multiple instances according to a map, or set of strings
- `provider`, for selecting a non-default provider configuration
- `lifecycle`, for lifecycle customizations
- `provisioner`, for taking extra actions after resource creation

---

# Practice

Let's deploy nginx container with Terraform:
- Create `practice_basics` and `cd` into it
- Create main.tf file
- Create provider block choosing docker provider
- Create `docker_image` resource
  - Choose `ghost` image with `latest` tag
- Create `docker_container` based on image chosen before-hand
  - Name the container `blog`
  - Provide the container with internal port of 2368 and external port of 80 
- Validate the container running and can be accessed
- Destroy the created infrastructure
> `[!]` Note: RTFM of [docker provider](https://registry.terraform.io/providers/kreuzwerker/docker/latest)

---

# Practice (cont.)

lets start by creating the files and folders
```sh
mkdir practice_basics && cd practice_basics
touch main.tf
```
lets add `main.tf` file with content:
```go
terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 3.0.1"
    }
  }
}

resource "docker_image" "ghost" {
  name = "ghost:latest"
}

resource "docker_container" "blog" {
  name  = "blog"
  image = docker_image.blog.name
  ports {
    internal = 2368
    external = 80
  }
}

```
---

# Practice (cont.)

Let us start by running the terraform commands and validate the run of the files

```sh
terraform init
terraform fmt
terraform validate
terraform plan
terraform apply
docker image ls
docker container ps -a 
curl localhost:80
terraform destroy 
```

---

# Terraform Configuration Language

### Taint and untaint resources

The meaning terraform `taint` is marking the resource for recreation during the next apply.
Terraform understands that this resource is in a bad state or needs to be replaced for some reason. The next time you run terraform apply, terraform will destroy the tainted resource and create a new one to replace it.

This is useful for situations where a resource may have become corrupted, misconfigured, or otherwise needs to be refreshed without modifying the entire state file or configuration

To use it simply create environment of containers with terraform and `taint` the resource in it:
```sh
cd ~/path/to/practice_basics
terraform apply
terraform show
terraform taint docker_container.container_id # this is taint
terraform plan # check for the changes in output
```
---
# Terraform Configuration Language

### Taint and untaint resources (cont.)

To cancel the `taint` on the container we can do it by the help of `untaint` command which removes the `taint` on the file:

```sh
# this is the continuation of the previous command set
terraform untaint docker_container.container_id # this is untaint
terraform plan
```

The consequence of the of the last command should be `No changes`, meaning that `taint` on the resource on the file has been removed.

Similarly we can achieve the same result of resource change (process that `taint` provides), by simply changing the `main.tf` file value:

```sh
cd ~/path/to/practice_basics
vi main.tf
```

```go
...
resource "docker_image" "ghost" {
  name = "ghost:alpine" // change "latest" to "alpine"
}
...
```

```sh
terraform validate
terraform plan # should show the change
terraform apply

```

---
> `[!]` Note: you may use  `terraform console` to get the values for complex variables
